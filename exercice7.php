<?php


// Définissez un petit programme permettant de savoir si les conditions sont bonnes pour aller surfer.
// Vous afficherez un petit message en conséquence. 
// Les données des variables ne sont volontairement pas définies (?) afin qu'on puisse les modifier. Votre programme doit toujours fonctionner.

// Si le vent est à plus de 30 km/h, alors il faut s'assurer que la houle ne dépasse pas les 20. 
// Si c'est le cas et que la candence des vagues est au moins de 10, on peut aller surfer.

// Si le vent est à moins de 30 km/h, alors la houle ne doit juste pas dépasser les 30. 
// Si c'est le cas, la cadence des vagues peut aller jusqu'à 8. 

// Dans les autres cas, les conditions ne sont pas bonnes pour aller surfer, affichez-le !



$vent = 21;
$houle = 1;
$cadence_vague =8 ;

if ($vent > 30) {

  if ($houle<20){

    if ($cadence_vague<10) {
      echo "on peut aller surfer";
    } else {
      echo "NON";
    }
  } else {
    echo "NON";
  }
}
else {
  if ($houle<30) {

    if ($cadence_vague<8) {

      echo "je peux aller surfer";
    } else {
      echo "NON";
    }
  } else {
    echo "NON";
  }
}

